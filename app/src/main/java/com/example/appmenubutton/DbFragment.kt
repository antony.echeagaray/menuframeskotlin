import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.Manifest
import android.graphics.Bitmap
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.example.appmenubutton.AlumnoLista
import com.example.appmenubutton.R
import com.example.appmenubutton.database.Alumno
import com.example.appmenubutton.database.dbAlumnos
import com.example.appmenubutton.database.definirTabla
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

class DbFragment : Fragment() {
    private lateinit var btnAdd: Button
    private lateinit var btnBuscar: Button
    private lateinit var btnDelete: Button
    private lateinit var txtMatricula: EditText
    private lateinit var txtNombre: EditText
    private lateinit var txtDomicilio: EditText
    private lateinit var txtEspecialidad: EditText
    private lateinit var txtUrlImagen: TextView
    private lateinit var imgAlumno: ImageView
    private lateinit var db: dbAlumnos
    private var imageUri: Uri? = null

    companion object {
        private const val REQUEST_CODE_PICK_IMAGE = 1000
        private const val REQUEST_CODE_STORAGE_PERMISSION = 1001
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_db, container, false)

        btnBuscar = view.findViewById(R.id.btnBuscar)
        btnAdd = view.findViewById(R.id.btnAdd)
        btnDelete = view.findViewById(R.id.btnBorrar)
        txtMatricula = view.findViewById(R.id.txtMatricula)
        txtEspecialidad = view.findViewById(R.id.txtEspecialidad)
        txtNombre = view.findViewById(R.id.txtNombre)
        txtDomicilio = view.findViewById(R.id.txtDomicilio)
        txtUrlImagen = view.findViewById(R.id.lblUrlImagen)
        imgAlumno = view.findViewById(R.id.imgAlumno)

        db = dbAlumnos(requireContext())
        db.openDataBase()

        // Eliminar registros sin matrícula
        db.eliminarRegistrosSinMatricula()

        requestStoragePermission()

        arguments?.let {
            val alumnoLista = it.getSerializable("mialumno") as AlumnoLista

            txtMatricula.setText(alumnoLista.matricula)
            txtNombre.setText(alumnoLista.nombre)
            txtDomicilio.setText(alumnoLista.domicilio)
            txtEspecialidad.setText(alumnoLista.especialidad)
            txtUrlImagen.setText(alumnoLista.foto)

            loadImage(alumnoLista.foto)
        }

        imgAlumno.setOnClickListener {
            pickImageFromGallery()
        }

        btnAdd.setOnClickListener {
            if (txtNombre.text.isNullOrEmpty() ||
                txtDomicilio.text.isNullOrEmpty() ||
                txtEspecialidad.text.isNullOrEmpty() ||
                txtMatricula.text.isNullOrEmpty()
            ) {
                Toast.makeText(requireContext(), "Falta información por capturar", Toast.LENGTH_SHORT).show()
            } else {
                val matricula = txtMatricula.text.toString()
                val alumnoExistente = db.getAlumno(matricula)
                val fotoUri = imageUri?.let { uri -> saveImageToInternalStorage(uri) } ?: "Pendiente"

                if (alumnoExistente.matricula.isNotEmpty()) {
                    alumnoExistente.apply {
                        nombre = txtNombre.text.toString()
                        domicilio = txtDomicilio.text.toString()
                        especialidad = txtEspecialidad.text.toString()
                        foto = fotoUri
                    }
                    db.actualizarAlumno(alumnoExistente)
                    Toast.makeText(requireContext(), "Se actualizó el alumno con matrícula $matricula", Toast.LENGTH_SHORT).show()
                } else {
                    val alumno = Alumno(
                        matricula = txtMatricula.text.toString(),
                        nombre = txtNombre.text.toString(),
                        domicilio = txtDomicilio.text.toString(),
                        especialidad = txtEspecialidad.text.toString(),
                        foto = fotoUri
                    )
                    val id: Long = db.insertarAlumno(alumno)
                    Toast.makeText(requireContext(), "Se agregó el alumno con ID $id", Toast.LENGTH_SHORT).show()
                }
                limpiarCampos()
            }
        }

        btnBuscar.setOnClickListener {
            if (txtMatricula.text.toString().isEmpty()) {
                Toast.makeText(requireContext(), "Falta capturar matrícula", Toast.LENGTH_SHORT).show()
            } else {
                val alumno = db.getAlumno(txtMatricula.text.toString())
                if (alumno.matricula.isNotEmpty()) {
                    txtNombre.setText(alumno.nombre)
                    txtDomicilio.setText(alumno.domicilio)
                    txtEspecialidad.setText(alumno.especialidad)
                    txtUrlImagen.text = alumno.foto
                    loadImage(alumno.foto)
                } else {
                    Toast.makeText(requireContext(), "No se encontró la matrícula", Toast.LENGTH_SHORT).show()
                }
            }
        }

        btnDelete.setOnClickListener {
            if (txtMatricula.text.toString().isEmpty()) {
                Toast.makeText(requireContext(), "Falta capturar matrícula", Toast.LENGTH_SHORT).show()
            } else {
                val matricula = txtMatricula.text.toString()
                AlertDialog.Builder(requireContext())
                    .setTitle("Confirmar eliminación")
                    .setMessage("¿Está seguro de que desea eliminar el alumno con matrícula $matricula?")
                    .setPositiveButton("Sí") { dialog, which ->
                        val deletedRows = db.borrarAlumno(matricula)
                        if (deletedRows > 0) {
                            Toast.makeText(requireContext(), "Se eliminó el alumno con matrícula $matricula", Toast.LENGTH_SHORT).show()
                            limpiarCampos()
                        } else {
                            Toast.makeText(requireContext(), "No se encontró la matrícula", Toast.LENGTH_SHORT).show()
                        }
                    }
                    .setNegativeButton("No", null)
                    .show()
            }
        }

        return view
    }

    private fun requestStoragePermission() {
        if (ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(requireActivity(), arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), REQUEST_CODE_STORAGE_PERMISSION)
        }
    }

    private fun pickImageFromGallery() {
        val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(intent, REQUEST_CODE_PICK_IMAGE)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_CODE_STORAGE_PERMISSION) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permiso concedido, podemos proceder
                loadImage(txtUrlImagen.text.toString())
            } else {
                Toast.makeText(requireContext(), "Permiso de almacenamiento denegado", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE_PICK_IMAGE && resultCode == Activity.RESULT_OK) {
            imageUri = data?.data
            try {
                val savedImagePath = saveImageToInternalStorage(imageUri!!)
                imgAlumno.setImageURI(imageUri)
                txtUrlImagen.text = savedImagePath
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }

    private fun loadImage(foto: String) {
        val file = File(foto)
        if (file.exists()) {
            imgAlumno.setImageURI(Uri.fromFile(file))
        }
    }


    private fun saveImageToInternalStorage(imageUri: Uri): String {
        val bitmap = MediaStore.Images.Media.getBitmap(requireActivity().contentResolver, imageUri)
        val filename = "${System.currentTimeMillis()}.jpg"
        val file = File(requireActivity().filesDir, filename)

        FileOutputStream(file).use { out ->
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out)
        }

        return file.absolutePath
    }

    private fun limpiarCampos() {
        txtMatricula.text.clear()
        txtNombre.text.clear()
        txtDomicilio.text.clear()
        txtEspecialidad.text.clear()
        txtUrlImagen.text = ""
        imgAlumno.setImageResource(R.drawable.image)
        imageUri = null
    }
}
