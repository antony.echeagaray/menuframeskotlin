package com.example.appmenubutton

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView

class HomeFragment : Fragment() {

    private lateinit var imageViewStudent: ImageView
    private lateinit var textViewName: TextView
    private lateinit var textViewSubject: TextView
    private lateinit var textViewDegree: TextView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_home, container, false)

        imageViewStudent = view.findViewById(R.id.imageViewStudent)
        textViewName = view.findViewById(R.id.textViewName)
        textViewSubject = view.findViewById(R.id.textViewSubject)
        textViewDegree = view.findViewById(R.id.textViewDegree)

        textViewName.text = "José Antonio de Jesús Osuna Echeagaray"
        textViewSubject.text = "Programación Movil"
        textViewDegree.text = "Tecnologías de la Información"

        return view
    }
}
