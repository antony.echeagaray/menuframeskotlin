package com.example.appmenubutton

import android.content.Context
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import de.hdodenhof.circleimageview.CircleImageView

class MiAdaptador(
    private val listaAlumnos: ArrayList<AlumnoLista>,
    private val context: Context
) : RecyclerView.Adapter<MiAdaptador.ViewHolder>() {
    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var listener: View.OnClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = inflater.inflate(R.layout.alumno_item, parent, false)
        return ViewHolder(view).apply {
            itemView.setOnClickListener {
                listener?.onClick(it)
            }
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val alumno = listaAlumnos[position]
        holder.txtMatricula.text = alumno.matricula
        holder.txtNombre.text = alumno.nombre
        holder.txtCarrera.text = alumno.especialidad

        // Mostrar la imagen usando la URI
        if (alumno.foto.startsWith("content://") || alumno.foto.startsWith("file://") || alumno.foto.startsWith("/data/")) {
            holder.idImagen.setImageURI(Uri.parse(alumno.foto))
        } else {
            try {
                holder.idImagen.setImageResource(alumno.foto.toInt())
            } catch (e: NumberFormatException) {
                holder.idImagen.setImageResource(R.drawable.image) // Placeholder en caso de error
            }
        }
    }

    override fun getItemCount(): Int {
        return listaAlumnos.size
    }

    fun setOnClickListener(listener: View.OnClickListener) {
        this.listener = listener
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val txtNombre: TextView = itemView.findViewById(R.id.txtAlumnoNombre)
        val txtMatricula: TextView = itemView.findViewById(R.id.txtMatricula)
        val txtCarrera: TextView = itemView.findViewById(R.id.txtCarrera)
        val idImagen: CircleImageView = itemView.findViewById(R.id.foto)
    }
}
