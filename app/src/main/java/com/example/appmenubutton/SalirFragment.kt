package com.example.appmenubutton

import DbFragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.appmenubutton.database.dbAlumnos
import com.google.android.material.floatingactionbutton.FloatingActionButton

class SalirFragment : Fragment() {
    private lateinit var rcvLista: RecyclerView
    private lateinit var adaptador: MiAdaptador
    private lateinit var btnNuevo: FloatingActionButton
    private lateinit var db: dbAlumnos

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_salir, container, false)

        // Inflate the layout for this fragment
        rcvLista = view.findViewById(R.id.recId)
        btnNuevo = view.findViewById(R.id.agregarAlumno)
        rcvLista.layoutManager = LinearLayoutManager(requireContext())

        // Inicializar la base de datos
        db = dbAlumnos(requireContext())
        db.openDataBase()

        // Obtener la lista de alumnos desde la base de datos
        val listaAlumno = db.leerTodos().map { alumno ->
            AlumnoLista(
                id = alumno.id,
                matricula = alumno.matricula,
                nombre = alumno.nombre,
                domicilio = alumno.domicilio,
                especialidad = alumno.especialidad,
                foto = alumno.foto  // Esto ahora es un String que puede ser una URI o un resource ID
            )
        }

        adaptador = MiAdaptador(ArrayList(listaAlumno), requireContext())
        rcvLista.adapter = adaptador

        btnNuevo.setOnClickListener {
            cambiarDbFragment()
        }

        adaptador.setOnClickListener(View.OnClickListener {
            val pos: Int = rcvLista.getChildAdapterPosition(it)
            val alumno = listaAlumno[pos]

            val bundle = Bundle().apply {
                putSerializable("mialumno", alumno)
            }

            val dbFragment = DbFragment().apply {
                arguments = bundle
            }

            parentFragmentManager.beginTransaction()
                .replace(R.id.frmContenedor, dbFragment)
                .addToBackStack(null)
                .commit()
        })

        return view
    }

    private fun cambiarDbFragment() {
        val cambioFragment = parentFragmentManager.beginTransaction()
        cambioFragment.replace(R.id.frmContenedor, DbFragment())
        cambioFragment.addToBackStack(null)
        cambioFragment.commit()
    }
}
