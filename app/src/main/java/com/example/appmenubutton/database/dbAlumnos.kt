package com.example.appmenubutton.database

import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import androidx.core.content.contentValuesOf

class dbAlumnos(private val context: Context) {

    private val dbHelper: AlumnosDbHelper = AlumnosDbHelper(context)
    private lateinit var db: SQLiteDatabase

    private val leerCampos = arrayOf(
        definirTabla.Alumnos.ID,
        definirTabla.Alumnos.MATRICULA,
        definirTabla.Alumnos.NOMBRE,
        definirTabla.Alumnos.DOMICILIO,
        definirTabla.Alumnos.ESPECIALIDAD,
        definirTabla.Alumnos.FOTO
    )

    fun openDataBase() {
        db = dbHelper.writableDatabase
    }

    fun insertarAlumno(alumno: Alumno): Long {
        val valores = contentValuesOf().apply {
            put(definirTabla.Alumnos.MATRICULA, alumno.matricula)
            put(definirTabla.Alumnos.NOMBRE, alumno.nombre)
            put(definirTabla.Alumnos.DOMICILIO, alumno.domicilio)
            put(definirTabla.Alumnos.ESPECIALIDAD, alumno.especialidad)
            put(definirTabla.Alumnos.FOTO, alumno.foto)
        }
        return db.insertWithOnConflict(definirTabla.Alumnos.TABLA, null, valores, SQLiteDatabase.CONFLICT_IGNORE)
    }

    fun actualizarAlumno(alumno: Alumno): Int {
        val valores = contentValuesOf().apply {
            put(definirTabla.Alumnos.MATRICULA, alumno.matricula)
            put(definirTabla.Alumnos.NOMBRE, alumno.nombre)
            put(definirTabla.Alumnos.DOMICILIO, alumno.domicilio)
            put(definirTabla.Alumnos.ESPECIALIDAD, alumno.especialidad)
            put(definirTabla.Alumnos.FOTO, alumno.foto)
        }
        return db.update(definirTabla.Alumnos.TABLA, valores,
            "${definirTabla.Alumnos.MATRICULA} = ?", arrayOf(alumno.matricula))
    }

    fun borrarAlumno(matricula: String): Int {
        return db.delete(definirTabla.Alumnos.TABLA, "${definirTabla.Alumnos.MATRICULA} = ?", arrayOf(matricula))
    }

    fun mostrarAlumnos(cursor: Cursor): Alumno {
        return Alumno().apply {
            id = cursor.getInt(0)
            matricula = cursor.getString(1)
            nombre = cursor.getString(2)
            domicilio = cursor.getString(3)
            especialidad = cursor.getString(4)
            foto = cursor.getString(5)
        }
    }

    fun getAlumno(matricula: String): Alumno {
        val db = dbHelper.readableDatabase
        val cursor = db.query(definirTabla.Alumnos.TABLA, leerCampos, "${definirTabla.Alumnos.MATRICULA} = ?", arrayOf(matricula), null, null, null)
        return if (cursor.moveToFirst()) {
            val alumno = mostrarAlumnos(cursor)
            cursor.close()
            alumno
        } else {
            cursor.close()
            Alumno() // Devuelve un alumno vacío si no se encuentra
        }
    }

    fun leerTodos(): ArrayList<Alumno> {
        val cursor = db.query(definirTabla.Alumnos.TABLA, leerCampos, null, null, null, null, null)
        val listaAlumno = ArrayList<Alumno>()
        cursor.moveToFirst()

        while (!cursor.isAfterLast) {
            val alumno = mostrarAlumnos(cursor)
            listaAlumno.add(alumno)
            cursor.moveToNext()
        }
        cursor.close()
        return listaAlumno
    }

    fun eliminarRegistrosSinMatricula(): Int {
        return db.delete(definirTabla.Alumnos.TABLA, "${definirTabla.Alumnos.MATRICULA} = ?", arrayOf(""))
    }

    fun close() {
        dbHelper.close()
    }
}
